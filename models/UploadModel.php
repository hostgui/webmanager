<?php
namespace WebManager\Models;

use WebManager\Lib\Config\AppConfig;
use WebManager\Lib\Config\GeneralConfig;
use WebManager\Lib\Database;
use WebManager\Lib\ErrorHandler;
use WebManager\Lib\Hooks;

class UploadModel {
    public const FIELDS = [
        'username' => 'string',
        'password' => 'string'
    ];

    public $username, $create_time, $update_time;
    private $password;

    /**
     * Hashes and sets the upload account password.
     * @param $password string
     */
    public function setPassword ($password) {
        $this->password = $this->hash($password);
    }

    /**
     * Hashes a password using the provided salt and workload factor.
     * @param $password string
     * @return string
     */
    private function hash ($password) {
        return crypt($password, '$2y$' . AppConfig::$general->hash_workload_factor . '$' . AppConfig::$general->hash_salt);
    }

    /**
     * Fetches a single upload account with provided username on a specific domain.
     * @param $domain string
     * @param $username string
     * @return UploadModel
     */
    public static function getSingle ($domain, $username) {
        $query = <<<EOD
                        SELECT www_upload.username,
                               www_upload.password,
                               www_upload.create_time,
                               www_upload.update_time
                        FROM www_upload,
                             www_domain
                        WHERE www_upload.domain_id = www_domain.id
                          AND www_upload.username = :username
                          AND www_domain.name = :domain
                    EOD;

        return Database::fetchSingleObj($query, self::class, [
            'domain' => $domain,
            'username' => $username
        ]);
    }

    /**
     * Fetches all upload accounts on a specific domain.
     * @param $domain string
     * @return UploadModel[]
     */
    public static function getAll ($domain) {
        $query = <<<EOD
                        SELECT www_upload.username,
                               www_upload.password,
                               www_upload.create_time,
                               www_upload.update_time
                        FROM www_upload,
                             www_domain
                        WHERE www_upload.domain_id = www_domain.id
                          AND www_domain.name = :domain
                    EOD;

        return Database::fetchAllObj($query, self::class, [ 'domain' => $domain ]);
    }

    public static function delete ($domain, $username) {
        Hooks::runPreDelete(Hooks::UPLOAD, [
            'domain' => $domain,
            'username' => $username
        ]);

        $query = <<<EOD
                        DELETE www_upload
                        FROM www_upload,
                             www_domain
                        WHERE www_upload.username = :username
                          AND www_upload.domain_id = www_domain.id
                          AND www_domain.name = :domain
                    EOD;

        Database::set($query, [
            'username' => $username,
            'domain' => $domain
        ]);

        Hooks::runPostDelete(Hooks::UPLOAD, [
            'domain' => $domain,
            'username' => $username
        ]);
    }

    /**
     * Adds a new upload account to the database.
     * @param $domain string
     */
    public function add ($domain) {
        Hooks::runPreCreate(Hooks::UPLOAD, [
            'username' => $this->username,
            'domain' => $domain
        ]);

        $query = <<<EOD
                        INSERT INTO www_upload (username,
                                                domain_id,
                                                password,
                                                create_time,
                                                update_time)
                        VALUES (:username,
                                (SELECT id FROM www_domain WHERE name = :domain),
                                :password,
                                CURRENT_TIME(),
                                CURRENT_TIME())
                    EOD;

        if ($this->checkDuplicate($domain))
            ErrorHandler::handle(409);

        Database::set($query, [
            'username' => $this->username,
            'password' => $this->password,
            'domain' => $domain
        ]);

        Hooks::runPostCreate(Hooks::UPLOAD, [
            'username' => $this->username,
            'domain' => $domain
        ]);
    }

    /**
     * Updates an upload account in the database.
     * @param $domain string
     * @param $old_username string
     */
    public function update ($domain, $old_username) {
        Hooks::runPreUpdate(Hooks::UPLOAD, [
            'username' => $this->username
        ]);

        $query = <<<EOD
                        UPDATE www_upload, www_domain
                        SET www_upload.username = :username,
                            www_upload.password = :password,
                            www_upload.update_time = CURRENT_TIME()
                        WHERE www_upload.domain_id = www_domain.id
                          AND www_upload.username = :old_username
                          AND www_domain.name = :domain
                    EOD;

        if ($old_username != $this->username && $this->checkDuplicate($domain))
            ErrorHandler::handle(409);

        Database::set($query, [
            'username' => $this->username,
            'password' => $this->password,
            'old_username' => $old_username,
            'domain' => $domain
        ]);

        Hooks::runPostUpdate(Hooks::UPLOAD, [
            'username' => $this->username
        ]);
    }

    /**
     * Checks whether an upload account with specified username-domain-combination already exists.
     * @param $domain string
     * @return bool
     */
    private function checkDuplicate ($domain) {
        $query = <<<EOD
                    SELECT COUNT(www_upload.username) AS count
                    FROM www_upload,
                         www_domain
                    WHERE www_upload.domain_id = www_domain.id
                      AND www_domain.name = :domain
                      AND www_upload.username = :username
                EOD;

        $res = Database::fetch($query, [ 'domain' => $domain, 'username' => $this->username ]);

        return $res['count'] > 0;
    }
}