<?php

namespace WebManager\Models;

use WebManager\Lib\Database;
use WebManager\Lib\ErrorHandler;
use WebManager\Lib\Hooks;

class DomainModel {
    public const FIELDS = [
        'name' => 'string',
        'quota' => 'integer'
    ];

    public $name, $quota, $path;

    /**
     * Fetches a list of all available domains.
     * @return DomainModel[]
     */
    public static function getAll () {
        $query = <<<EOD
                        SELECT name, quota, path, create_time, update_time
                        FROM www_domain
                    EOD;

        return Database::fetchAllObj($query, self::class);
    }

    /**
     * Fetches a single domain.
     * @param $domain string
     * @return DomainModel
     */
    public static function getSingle ($domain) {
        $query = <<<EOD
                        SELECT name, quota, path, create_time, update_time
                        FROM www_domain
                        WHERE www_domain.name = :domain
                    EOD;

        return Database::fetchSingleObj($query, self::class, ['domain' => $domain]);
    }

    /**
     * Adds a new domain.
     */
    public function add () {
        Hooks::runPreCreate(Hooks::DOMAIN, [
            'name' => $this->name,
            'quota' => $this->quota,
            'path' => $this->path
        ]);

        $query = <<<EOD
                        INSERT INTO www_domain (name,
                                                quota,
                                                path,
                                                create_time,
                                                update_time)
                        VALUES (:name,
                                :quota,
                                :path,
                                CURRENT_TIME(),
                                CURRENT_TIME())
                    EOD;

        if ($this->checkDuplicate())
            ErrorHandler::handle(409);

        Database::set($query, [
            'name' => $this->name,
            'quota' => $this->quota,
            'path' => $this->path
        ]);

        Hooks::runPostCreate(Hooks::DOMAIN, [
            'name' => $this->name,
            'quota' => $this->quota,
            'path' => $this->path
        ]);
    }

    /**
     * Updates a domain in the database.
     * @param $old_name string
     */
    public function update ($old_name) {
        Hooks::runPreUpdate(Hooks::DOMAIN, [
            'name' => $this->name,
            'quota' => $this->quota,
            'path' => $this->path
        ]);

        $query = <<<EOD
                        UPDATE www_domain
                        SET www_domain.name = :name,
                            www_domain.quota = :quota,
                            www_domain.path = :path,
                            www_domain.update_time = CURRENT_TIME()
                        WHERE www_domain.name = :old_name
                    EOD;

        if ($old_name != $this->name && $this->checkDuplicate())
            ErrorHandler::handle(409);

        Database::set($query, [
            'name' => $this->name,
            'quota' => $this->quota,
            'path' => $this->path,
            'old_name' => $old_name
        ]);

        Hooks::runPostUpdate(Hooks::DOMAIN, [
            'name' => $this->name,
            'quota' => $this->quota,
            'path' => $this->path
        ]);
    }

    /**
     * Deletes a domain from the database.
     * The database must be configured so that rows in other tables referencing this domain in get automatically deleted as well!
     * @param $domain string
     */
    public static function delete ($domain) {
        Hooks::runPreDelete(Hooks::DOMAIN, [
            'domain' => $domain
        ]);

        $query = <<<EOD
                        DELETE www_domain
                        FROM www_domain
                        WHERE www_domain.name = :domain
                    EOD;

        Database::set($query, ['domain' => $domain]);

        Hooks::runPostDelete(Hooks::DOMAIN, [
            'domain' => $domain
        ]);
    }

    /**
     * Checks whether a domain with specified name already exists.
     * @return bool
     */
    private function checkDuplicate () {
        $query = <<<EOD
                        SELECT COUNT(www_domain.name) AS count
                        FROM www_domain
                        WHERE www_domain.name = :domain
                    EOD;

        $res = Database::fetch($query, ['domain' => $this->name]);

        return $res['count'] > 0;
    }
}