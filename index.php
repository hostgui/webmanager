<?php
namespace WebManager;

require 'vendor/autoload.php';

use WebManager\Lib\Config\AppConfig;
use WebManager\Lib\Database;
use WebManager\Lib\Hooks;
use WebManager\Lib\Router;
use WebManager\Lib\WebConfigurator;

AppConfig::init([
    '/etc/hosting/emailmanager.toml',
    '/etc/emailmanager.toml',
    __DIR__ . '/config.toml'
]);

Database::init();
WebConfigurator::init();
Hooks::init();

$router = new Router();
$router->start();