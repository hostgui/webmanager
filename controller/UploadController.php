<?php
namespace WebManager\Controller;

use WebManager\Lib\ErrorHandler;
use WebManager\Models\DomainModel;
use WebManager\Models\UploadModel;
use Flight;

class UploadController {
    public static function get ($domain) {
        if (DomainModel::getSingle($domain) == null)
            ErrorHandler::handle(404);

        $data = UploadModel::getAll($domain);

        echo json_encode($data);
    }

    public static function create ($domain) {
        $data = Flight::request()->data;

        foreach (UploadModel::FIELDS as $key => $value) {
            if (!isset($data[$key]) || gettype($data[$key]) != $value)
                ErrorHandler::handle(400);
        }

        $upload = new UploadModel();
        $upload->username = $data['username'];
        $upload->setPassword($data['password']);

        $upload->add($domain);

        echo RESULT_OK;
    }

    public static function update ($domain, $username) {
        $data = Flight::request()->data;

        foreach (UploadModel::FIELDS as $key => $value) {
            if (isset($data[$key]) && gettype($data[$key]) != $value)
                ErrorHandler::handle(400);
        }

        $upload = UploadModel::getSingle($domain, $username);
        if ($upload == null)
            ErrorHandler::handle(404);

        foreach ($data as $key => $value) {
            switch ($key) {
                case 'username':
                    $upload->username = $value;
                    break;
                case 'password':
                    $upload->setPassword($value);
                    break;
            }
        }

        $upload->update($domain, $username);

        echo RESULT_OK;
    }

    public static function delete ($domain, $username) {
        if (UploadModel::getSingle($domain, $username) == null)
            ErrorHandler::handle(404);

        UploadModel::delete($domain, $username);

        echo RESULT_OK;
    }
}