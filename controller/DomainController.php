<?php
namespace WebManager\Controller;

use WebManager\Lib\WebConfigurator;
use WebManager\Models\DomainModel;
use WebManager\Lib\ErrorHandler;
use Flight;

class DomainController {
    public static function get () {
        $data = DomainModel::getAll();

        echo json_encode($data);
    }

    public static function create () {
        $data = Flight::request()->data;

        foreach (DomainModel::FIELDS as $key => $value) {
            if (!isset($data[$key]) || gettype($data[$key]) != $value)
                ErrorHandler::handle(400);
        }

        $domain = new DomainModel();
        $domain->name = $data['name'];
        $domain->quota = $data['quota'];
        $domain->path = uniqid();

        WebConfigurator::configure($domain);
        $domain->add();

        echo RESULT_OK;
    }

    public static function update ($hostname) {
        $data = Flight::request()->data;

        foreach (DomainModel::FIELDS as $key => $value) {
            if (isset($data[$key]) && gettype($data[$key]) != $value)
                ErrorHandler::handle(400);
        }

        $domain = DomainModel::getSingle($hostname);

        if ($domain == null)
            ErrorHandler::handle(404);

        foreach ($data as $key => $value) {
            switch ($key) {
                case 'name':
                    $domain->name = $value;
                    break;
                case 'quota':
                    $domain->quota = $value;
                    break;
            }
        }

        WebConfigurator::configure($domain);
        $domain->update($hostname);

        echo RESULT_OK;
    }

    public static function delete ($domain) {
        if (DomainModel::getSingle($domain) == null)
            ErrorHandler::handle(404);

        WebConfigurator::delete($domain);
        DomainModel::delete($domain);

        echo RESULT_OK;
    }
}