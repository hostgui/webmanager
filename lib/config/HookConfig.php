<?php
namespace WebManager\Lib\Config;

use WebManager\Lib\ErrorHandler;
use WebManager\Lib\Hooks;

class HookConfig extends Config {
    protected $location = 'hook';
    protected $types = [
        'required' => self::BOOLEAN,
        'operation' => self::STRING,
        'action' => self::STRING,
        'execute' => self::STRING
    ];

    private $operation;
    public $required = false,
        $model,
        $cud,
        $action,
        $execute;

    public function parse ($config) {
        $this->checkAndSet($config, 'required', $this->required);
        $this->checkAndSet($config, 'action', $this->action);
        $this->checkAndSet($config, 'operation', $this->operation);
        $this->checkAndSet($config, 'execute', $this->execute);

        $this->parseOperation();
    }

    public function check () {
        $this->mustBeSet($this->operation, 'operation');
        $this->mustBeSet($this->action, 'action', null, 'hook "' . $this->operation . '"');
        $this->mustBeSet($this->execute, 'execute', null, 'hook "' . $this->operation . '"');

        if (!in_array($this->action, Hooks::ACTIONS))
            $this->throwError('Option "action" must be either one of: :actions',
                [ ':actions' => join(', ', Hooks::ACTIONS) ],
                'hook ' . $this->operation);
    }

    private function parseOperation () {
        $operation = explode('.', $this->operation);
        if (count($operation) != 2)
            $this->throwError('Option "operation" must be in the form "model.crud" (e.g. "account.create")',
                null,
                'hook ' . $this->operation);

        if (!in_array($operation[0], Hooks::MODELS))
            $this->throwError('A hook operation model must be either one of: :models',
                [ ':models' => join(', ', Hooks::MODELS) ],
                'hook ' . $this->operation);

        if (!in_array($operation[1], Hooks::CUD))
            $this->throwError('A CUD operation can be either one of: :cud',
                [ ':cud' => join(', ', Hooks::CUD) ],
                'hook ' . $this->operation);

        $this->model = $operation[0];
        $this->cud = $operation[1];
    }
}