<?php
namespace WebManager\Lib\Config;

class WebserverConfig extends Config {
    protected $location = 'webserver';
    protected $types = [
        'config_path' => self::STRING,
        'root_path' => self::STRING,
        'template' => self::STRING
    ];

    public $config_path,
        $template,
        $root_path;

    public function parse ($config) {
        $this->checkAndSet($config, 'config_path', $this->config_path);
        $this->checkAndSet($config, 'root_path', $this->root_path);
        $this->checkAndSet($config, 'template', $this->template);
    }

    public function check () {
        $this->mustBeSet($this->config_path, 'config_path');
        $this->mustBeSet($this->root_path, 'root_path');
        $this->mustBeSet($this->template, 'template');

        if (!is_writable($this->config_path))
            $this->throwError('The webserver config directory ":config" is not writable.', [
                ':config' => $this->config_path
            ]);

        if (!is_dir($this->config_path))
            $this->throwError('The webserver config directory ":config" is a file.', [
                ':config' => $this->config_path
            ]);

        if (!is_writable($this->root_path))
            $this->throwError('The root directory ":root" is not writable.', [
                ':root' => $this->root_path
            ]);

        if (!is_dir($this->root_path))
            $this->throwError('The root directory ":root" is a file.', [
                ':root' => $this->root_path
            ]);

        if (!is_readable($this->template))
            $this->throwError('The template file ":template" is not readable.', [
                ':template' => $this->template
            ]);

        if (is_dir($this->template))
            $this->throwError('The template file ":template" is a directory.', [
                ':template' => $this->template
            ]);
    }
}