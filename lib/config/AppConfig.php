<?php
namespace WebManager\Lib\Config;

use WebManager\Lib\ErrorHandler;
use Yosymfony\Toml\Exception\ParseException;
use Yosymfony\Toml\Toml;

class AppConfig extends Config {
    /** @var GeneralConfig $general */
    public static $general;
    /** @var DBConfig $database */
    public static $database;
    /** @var WebserverConfig $webserver */
    public static $webserver;
    /** @var HookConfig[] $hooks */
    public static $hooks;

    /** @var GeneralConfig $m_general */
    private $m_general;
    /** @var DBConfig $m_database */
    private $m_database;
    /** @var WebserverConfig $m_webserver */
    private $m_webserver;
    /** @var HookConfig[] $m_hooks */
    private $m_hooks = [];

    public function __construct () {
        $this->m_general = new GeneralConfig();
        $this->m_database = new DBConfig();
        $this->m_webserver = new WebserverConfig();
    }

    /**
     * Get an Config instance from an array of potential configuration paths.
     * @param $locations string[] An array of configuration paths that should be checked.
     */
    public static function init ($locations) {
        foreach ($locations as $location)
            if (file_exists($location)) {
                $path = $location;
                break;
            }

        if (!isset($path))
            ErrorHandler::throw('No configuration file was provided.');

        $app_config = new AppConfig();
        $app_config->parseFile($path);
        $app_config->check();

        self::$general = $app_config->m_general;
        self::$database = $app_config->m_database;
        self::$webserver = $app_config->m_webserver;
        self::$hooks = $app_config->m_hooks;
    }

    /**
     * Parses a configuration file.
     * @param $path string
     */
    public function parseFile ($path) {
        try {
            $config = Toml::parseFile($path);
        } catch (ParseException $e) {
            ErrorHandler::throw($e);
        }

        if (!isset($config))
            $this->throwError('Configuration file ":path" could not be parsed. Check the syntax.', [':path' => $path]);

        if (isset($config['include']))
            foreach ($config['include'] as $include) {
                $this->mustBeSet($include['file'], 'file', null, 'include');
                $this->enforceType($include['file'], 'file', 'string', 'include');

                $include_path = $include['file'];

                if (!file_exists($include_path))
                    $this->throwError('Included file ":include_path" does not exist.', ['include_path' => $include_path]);

                $this->parseFile($include_path);
            }

        $this->parse($config);
    }

    public function parse ($config) {
        if (isset($config['general']))
            $this->m_general->parse($config['general']);

        if (isset($config['database']))
            $this->m_database->parse($config['database']);

        if (isset($config['webserver']))
            $this->m_webserver->parse($config['webserver']);

        if (isset($config['hook']))
            foreach ($config['hook'] as $hook) {
                $hook_config = new HookConfig();
                $hook_config->parse($hook);

                array_push($this->m_hooks, $hook_config);
            }
    }

    public function check () {
        $this->m_general->check();
        $this->m_database->check();
        $this->m_webserver->check();

        /** @var HookConfig $hook */
        foreach ($this->m_hooks as $hook) {
            $hook->check();
        }
    }
}