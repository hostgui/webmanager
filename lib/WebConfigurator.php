<?php
namespace WebManager\Lib;

use Twig_Environment;
use Twig_Error_Loader;
use Twig_Error_Runtime;
use Twig_Error_Syntax;
use Twig_Loader_Array;
use WebManager\Lib\Config\AppConfig;
use WebManager\Models\DomainModel;

class WebConfigurator {
    /** @var Twig_Environment $twig */
    private static $twig;

    /**
     * Initiates Twig.
     */
    public static function init () {
        $loader = new Twig_Loader_Array([
            'template' => file_get_contents(AppConfig::$webserver->template)
        ]);

        self::$twig = new Twig_Environment($loader, [
            'cache' => './cache'
        ]);
    }

    /**
     * Configures a domain, creating a www directory as well as generating a configuration file.
     * @param $domain DomainModel
     */
    public static function configure ($domain) {
        $root = AppConfig::$webserver->root_path . '/' . $domain->path;
        $config = AppConfig::$webserver->config_path . '/' . $domain->path . '.conf';

        if (!file_exists($root))
            mkdir($root);

        try {
            file_put_contents($config, self::$twig->render('template', [
                'root' => $root, 'hostname' => $domain->name
            ]));
        } catch (Twig_Error_Loader $e) {
            ErrorHandler::handle(500, 'There was an error loading the configured template file.');
        } catch (Twig_Error_Runtime $e) {
            ErrorHandler::handle(500, 'There was a runtime error rendering the configured template file.');
        } catch (Twig_Error_Syntax $e) {
            ErrorHandler::handle(500, 'There is a syntax error in the configured template file.');
        }
    }

    /**
     * Deletes a domain from the file system.
     * @param $domain DomainModel
     */
    public static function delete ($domain) {
        self::remove(AppConfig::$webserver->root_path . '/' . $domain->path);
        unlink(AppConfig::$webserver->config_path . '/' . $domain->path . '.conf');
    }

    /**
     * Removes a directory recursively-
     * @param $path string
     */
    private static function remove ($path) {
        if (!is_dir($path)) {
            @unlink($path);
            return;
        }

        $dir = opendir($path);

        while (false !== ($file = readdir($dir))) {
            if ($file != '.' && $file != '..')
                if (is_dir($path . '/' . $file))
                    self::remove($path . '/' . $file);
                else
                    unlink($path . '/' . $file);
        }

        closedir($dir);

        rmdir($path);
    }
}