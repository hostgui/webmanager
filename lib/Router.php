<?php
namespace WebManager\Lib;

use WebManager\Controller\DomainController;
use WebManager\Controller\UploadController;
use WebManager\Lib\Config\AppConfig;
use Flight;

class Router {
    function __construct () {
        header('Content-Type: application/json');

        define('RESULT_OK', json_encode([ 'message' => 'ok' ]));

        // Check for API key header
        Flight::route('*', function () {
            $headers = getallheaders();

            if (!isset($headers['X-Api-Key']))
                ErrorHandler::handle(401);

            if ($headers['X-Api-Key'] != AppConfig::$general->api_key)
                ErrorHandler::handle(401);

            return true;
        });

        // Setup routes
        Flight::route('GET /api/domain', [DomainController::class, 'get']);
        Flight::route('POST /api/domain', [DomainController::class, 'create']);
        Flight::route('POST /api/domain/@domain', [DomainController::class, 'update']);
        Flight::route('DELETE /api/domain/@domain', [DomainController::class, 'delete']);

        Flight::route('GET /api/upload/@domain', [UploadController::class, 'get']);
        Flight::route('POST /api/upload/@domain', [UploadController::class, 'create']);
        Flight::route('POST /api/upload/@domain/@username', [UploadController::class, 'update']);
        Flight::route('DELETE /api/upload/@domain/@username', [UploadController::class, 'delete']);

        // Respond with 404 to every other request
        Flight::route('*', function () {
            ErrorHandler::handle(404);
        });
    }

    public function start () {
        Flight::start();
    }
}