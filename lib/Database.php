<?php
namespace WebManager\Lib;

use WebManager\Lib\Config\AppConfig;
use \PDO;
use \PDOException;

class Database {
    private const DEFAULT_OPTIONS = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_PERSISTENT => false,
        PDO::ATTR_EMULATE_PREPARES => false,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    ];

    /** @var PDO $pdo */
    private static $pdo;

    public static function init ($options = self::DEFAULT_OPTIONS) {
        $config = AppConfig::$database;

        $host = $config->host;
        $port = $config->port;
        $dbname = $config->dbname;
        $username = $config->username;
        $password = $config->password;

        try {
            self::$pdo = new PDO("mysql:host=$host;dbname=$dbname;port=$port;charset=utf8mb4", $username, $password, $options);
        } catch (PDOException $e) {
            ErrorHandler::throw($e);
        }
    }

    /**
     * Fetches a single row without returning a class instance.
     * @param $query string Query.
     * @param $params array Parameters passed to the prepared query.
     * @return mixed
     */
    public static function fetch ($query, $params) {
        $p = self::prepareParams($params);

        try {
            $statement = self::$pdo->prepare($query);
            $statement->execute($p);
        } catch (PDOException $e) {
            ErrorHandler::handlePDO($e);
        }

        return $statement->fetch();
    }

    /**
     * Fetches multiple rows without returning class instances.
     * @param $query string Query.
     * @param $params array Parameters passed to the prepared query.
     * @param $single_column bool Whether the returned array should be one-dimansional.
     * @return array
     */
    public static function fetchAll ($query, $params, $single_column = false) {
        $p = self::prepareParams($params);

        try {
            $statement = self::$pdo->prepare($query);
            $statement->execute($p);
        } catch (PDOException $e) {
            ErrorHandler::handlePDO($e);
        }

        return $statement->fetchAll($single_column ? PDO::FETCH_COLUMN : null);
    }

    /**
     * Inserts or updates rows in the database.
     * @param $query string INSERT query.
     * @param $params array Parameters passed to the prepared query.
     */
    public static function set ($query, $params) {
        $p = self::prepareParams($params);

        try {
            self::$pdo->prepare($query)->execute($p);
        } catch (PDOException $e) {
            ErrorHandler::handlePDO($e);
        }
    }

    /**
     * Fetches a single row in a table.
     * @param $query string SELECT query.
     * @param $class string FQCN of the returned objects.
     * @param $params array Parameters passed to the prepared query.
     * @return mixed An instance of the specified class.
     */
    public static function fetchSingleObj ($query, $class, $params = []) {
        $p = self::prepareParams($params);

        try {
            $statement = self::$pdo->prepare($query);
            $statement->execute($p);
        } catch (PDOException $e) {
            ErrorHandler::handlePDO($e);
        }

        return $statement->fetchObject($class);
    }

    /**
     * Fetches multiple rows in a table.
     * @param $query string SELECT query.
     * @param $class string FQCN of the returned objects.
     * @param $params array Parameters passed to the prepared query.
     * @return array An array of instances of the specified class.
     */
    public static function fetchAllObj ($query, $class, $params = []) {
        $p = self::prepareParams($params);

        try {
            $statement = self::$pdo->prepare($query);
            $statement->execute($p);
        } catch (PDOException $e) {
            ErrorHandler::handlePDO($e);
        }

        return $statement->fetchAll(PDO::FETCH_CLASS, $class);
    }

    private static function prepareParams ($params) {
        foreach ($params as $key => $value) {
            if (is_bool($value)) {
                $params[$key] = ($value ? 1 : 0);
            }
        }

        return $params;
    }
}